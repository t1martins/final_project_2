import Vue from 'vue'
import Vuex from 'vuex'
//not used
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
